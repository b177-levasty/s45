import React from 'react';
  
  {/* Hindi ko po mapagana yung sa refactor ng banner kaya po nag gawa na lang ako ng bagong page para sa Page not found Sir */}
export default function PageNotFound() {

    return(
        <div>
            <h1>Page Not Found</h1>
            <h3>Go back to <a href="/">Homepage</a></h3>
        </div>
    )
}
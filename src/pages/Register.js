import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Login from './Login';
/*https://react-bootstrap.github.io/forms/overview/#overview*/
	
	export default function Register(){
		// State hooks to store the values of the input fields
		// Activity
		const { user, setUser } = useContext(UserContext);
		const [firstName, setFirstName] = useState('');
		const [lastName, setLastName] = useState('');
		const [mobileNo, setMobileNo] = useState('');
		const [email, setEmail] = useState('');
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2] = useState('');
		// State to determine wheyher submit botton is enabled or not
		const [isActive, setIsActive] = useState(false);

		// Check if values are successfully binded
		console.log(email);
		console.log(password1);
		console.log(password2);	

		function registerUser(e){
			e.preventDefault();

			fetch('http://localhost:4000/users/register', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                	firstName: firstName,
                	lastName: lastName,
                    email: email,
                    mobileNo: mobileNo,
                    password: password1
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)

            
				Swal.fire({
                        title: "Register Successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })
				setFirstName('');
				setLastName('');
				setEmail('');
				setMobileNo('');
				setPassword1('');
				setPassword2('');
			   
			})
		

			// Clear input fields
			

			// if(data === email){
			// 	Swal.fire({
   //                      title: "Register Successful",
   //                      icon: "success",
   //                      text: "Welcome to Zuitt!"
   //                  })
			// 	setFirstName('');
			// 	setLastName('');
			// 	setEmail('');
			// 	setMobileNo('');
			// 	setPassword1('');
			// 	setPassword2('');
			// }
			// else{
			// 	Swal.fire({
			// 	    title: "Register failed",
			// 	    icon: "error",
			// 	    text: "Try again."
			// 	})
			// }
			
		}

		useEffect(() => {
			// Validation to enable submit button when all fields are populated and both passwords match
			if((firstName !== '' && lastName !== '' && mobileNo !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		}, [firstName, lastName, mobileNo, email, password1, password2]);


	return(
		//Activity
		(user.id !== null) ?
		    <Navigate to="/login" />
		:
		<Form onSubmit={(e) => registerUser(e)}>
		  <h1>Register</h1>
		  <Form.Group className="mb-3" controlId="userEmail">

		  	<Form.Label>First Name:</Form.Label>
		  	<Form.Control 
		  		type="text" 
		  		placeholder="Enter first name" 
		  		value={firstName}
		  		onChange={e => setFirstName(e.target.value)}
		  		required
		  	/>
		    <Form.Label>Last Name:</Form.Label>
		    <Form.Control 
		    	type="text" 
		    	placeholder="Enter email" 
		    	value={lastName}
		    	onChange={e => setLastName(e.target.value)}
		    	required
		    />
		    <Form.Label>Email Address:</Form.Label>
		    <Form.Control 
		    	type="email" 
		    	placeholder="Enter email" 
		    	value={email}
		    	onChange={e => setEmail(e.target.value)}
		    	required
		    />
		    <Form.Label>Mobile Number:</Form.Label>
		    <Form.Control 
		    	type="tel"
		    	maxLength="11" 
		    	placeholder="Enter mobile" 
		    	value={mobileNo}
		    	onChange={e => setMobileNo(e.target.value)}
		    	required
		    />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>
		{/*Password1*/}
		  <Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password"
		    	value={password1}
		    	onChange={e => setPassword1(e.target.value)}
		    	required 
		    />
		  </Form.Group>
		{/*Password2*/}
		  <Form.Group className="mb-3" controlId="password2">
		    <Form.Label>Verify Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Verify Password"
		    	value={password2}
		    	onChange={e => setPassword2(e.target.value)}
		    	required 
		    />
		  </Form.Group>
		  {/* Conditionally render submit button based on isActive state */}
		  { isActive ?
		  	<Button variant="primary" type="submit" id="submitBtn">
		  	  Submit
		  	</Button>
		  	:
		  	<Button variant="danger" type="submit" id="submitBtn" disabled>
		  	  Submit
		  	</Button>
		  }
		  
		</Form>
	)
}
// Activity
/*import { Row, Col, Card } from 'react-bootstrap';
import { Button } from 'react-bootstrap';

export default function Highlights(){
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card style={{ width: '18rem' }} className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>
				    	<h2>JavaScript</h2>
				    </Card.Title>
				    <Card.Text>
				     	<h6>Description:</h6>
				     	<h6>An object-oriented computer programming language commonly used to create interactive effects within web browsers.</h6>
				    </Card.Text>
				    <Card.Text>
				     	<h6>Price:</h6>
				     	<h6>PhP 40,000</h6>
				    </Card.Text>
				    <Button variant="primary">Enroll</Button>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}*/

import { useState} from 'react';
import PropTypes from 'prop-types';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {
	// console.log(props);
	// console.log(typeof props);

	const {_id, name, description, price} = courseProp;

	// Use the state hook for this component to be able to store its state
	// State are used to keep track of information related to individual components
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue);

	// const [count, setCount] = useState(0);
	// console.log(useState(0));
	// const [seatCount, setSeat] = useState(30);
	// console.log(useState(30));

	// Function that keeps track of enrollees for a course
	// function enroll(){
	// 	setCount(count + 1);
	// 	setSeat(seatCount - 1);
	// 	if(count >= 30){
	// 		alert('No more seats!');
	// 		setCount(30);
	// 		setSeat(0)
	// 	}
	// 	console.log('Enrollees ' + count);
	// 	console.log('Seats ' + seatCount)
	// }

	// const[seatCount, setSeat] = useState(30);
	// console.log(useState(30));

	// function seats(){
	// 	setSeat(seatCount - 1)
	// 	console.log('Seats ' + seatCount)
	// }




    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Button variant="primary" to={`/courses/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
    )
}

// Check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}

